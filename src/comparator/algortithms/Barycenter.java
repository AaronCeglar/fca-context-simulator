package comparator.algortithms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.BitSet;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import Utilities.Context;

public class Barycenter 
{
	private Map<String, node> attrs = new HashMap<String, node>();
	private Map<String, node> objs = new HashMap<String, node>();

	private Set<node> ObjSet = new TreeSet<node>(new Comparator<node>()
	{
		@Override
		public int compare(node a, node b) {
			return a.score >= b.score?1:-1;
		}
	}); 

	private Set<node> AttSet = new TreeSet<node>(new Comparator<node>() 
	{
		@Override
		public int compare(node a, node b) {
			return a.score >= b.score?1:-1;
		}
	}); 
	
	public Barycenter() {}

	public void buildStructuresFromFile(String file) 
	{
		ClassLoader classLoader = getClass().getClassLoader();
		try {
			System.out.println("reading file");

			InputStream in = classLoader.getResourceAsStream(file);
			BufferedReader b = new BufferedReader(new InputStreamReader(in));

			int objId=0;
			String line;
			attrs = new HashMap<String, node>();
			objs = new HashMap<String, node>();
			while ((line = b.readLine()) != null) 
			{
				line = line.trim();
				if(line.length() == 0) 
					continue;
				String[] sa = line.split(" ");

				addObject(objId);
				for(int i=0; i<sa.length;i++) 
				{
					if(isNum(1.0, sa[i]))
					{
						addAttribute(i);
						addAssociation(objId, i);
					}
				}
				objId++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void buildStructuresFromContext(Context cont) 
	{
		List<String> objLabels = cont.getObjLabels();
		for(int index=0; index<objLabels.size();index++)
		{
			String objLab = objLabels.get(index);
			BitSet b = cont.getObjAttrs(index);
			
			addObject(objLab);
			
			//attributes for the object
			for (int i = b.nextSetBit(0); i >= 0; i = b.nextSetBit(i+1))
			{
				addAttribute(i);
				addAssociation(objLab,String.valueOf(i));
			}
		}
	}
	

	public void addObject(int id)
	{
		addObject(String.valueOf(id));
	}

	public void addObject(String id)
	{
		if(objs.containsKey(id))
			return;

		objs.put(id, new node(id,objs.size()));
	}


	public void addAttribute(int id)
	{
		addAttribute(String.valueOf(id));
	}

	public void addAttribute(String id)
	{
		if(attrs.containsKey(id))
			return;

		attrs.put(id, new node(id,attrs.size()));
	}

	public void addAssociation(int objId, int attId)
	{
		addAssociation(String.valueOf(objId), String.valueOf(attId));
	}

	public void addAssociation(String objId, String attId)
	{
		objs.get(objId).associates.add(attrs.get(attId));
		attrs.get(attId).associates.add(objs.get(objId));
	}

	public boolean isNum(double x, String strNum)
	{
		double d = 0.0; 
		try {
			d = Double.parseDouble(strNum);
		}catch (NumberFormatException e) {
			return false;
		}
		if(d == x) return true;

		return false;
	}

	public void sort() 
	{
	//	System.out.println("objs: "+objs.values());
	//	System.out.println("atts: "+attrs.values());
		System.out.println("sorting");
		for (int i=0; i<10; i++)
		{
			AtomSort(objs);
			AtomSort(attrs);
		//	System.out.println("objs: "+objs.values());
		//	System.out.println("atts: "+attrs.values());
		}

		ObjSet.addAll(objs.values());
		AttSet.addAll(attrs.values());

		//	System.out.println(objs);
	}

	// will update the map contents with new ordering.
	private void AtomSort(Map<String, node> map)
	{
		SortedSet<node> sorted = new TreeSet<node>(new Comparator<node>() {
			@Override
			public int compare(node a, node b) {
				return a.score >= b.score ? 1 : -1;
			}
		}); 

		//for each node (atom || coatom)
		for(node n: map.values()) // for each element
		{
			double ci = 0.0d;
			//iterate over its values (coatoms || atoms) accruing their scores(ranks)
			for (node a: n.associates)
				ci+=a.score;
			ci = (ci / n.associates.size());// divide by the number of values to obtain the final score.

			n.setScore(ci);
			sorted.add(n);
		}

		int i=0;
		for(node c: sorted)
			c.setScore(i++);
	}

	public void printResults()
	{
		printObjs();
		printAttrs();
	}

	public void printObjs()
	{
		System.out.println("Object set");
		print(ObjSet);
	}

	public void printAttrs()
	{
		System.out.println("Attribute Set");
		print(AttSet);
	}

	private void print(Set<node> s)
	{
		for(node n: s)
		{
			System.out.println(n);
		}
	}

	public static void main(String[] args)
	{
		Barycenter bc = new Barycenter();
		bc.buildStructuresFromFile("./resources/Contexts/context42-atomic.txt");
		bc.sort();
		bc.printResults();
	}
}

class node extends Object
{
	String id;
	double score;
	Set<node> associates = new TreeSet<node>(new Comparator<node>() 
	{
		@Override
		public int compare(node a, node b) 
		{
			return (a.id.equals(b.id))?0:1;
		}
	});


	public node(int id)
	{
		this(String.valueOf(id));
	}

	public node(String id)
	{
		this.id = id;
	}


	public node(int id, int score) 
	{
		this(String.valueOf(id), score);
	}


	public node(String id, int score) 
	{
		this.id = id;
		this.score = score;
	}


	public void setScore(double s) 
	{
		score = s; 
	}

	@Override 
	public boolean equals(Object o)
	{
		if (!(o instanceof node))
			return false;

		node n = (node)o;
		if(n.id == this.id)
			return true;

		return false;
	}

	@Override
	public int hashCode()
	{
		return id.hashCode();
	}

	public void addAssociate(node n)
	{
		associates.add(n);
	}

	public String toString()
	{
		return id+" ["+(int)score+"] ";
	}

	public String getNodeAndAssociatesString()
	{
		StringBuffer sb = new StringBuffer();

		sb.append(this.toString());
		for(node n: associates)
		{
			sb.append(" : "+n.id+" ");
		}
		return sb.toString();
	}

}

