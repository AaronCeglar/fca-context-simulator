package comparator.algortithms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import Utilities.Context;
import mdsj.MDSJ;

public class JamaResistanceLayout 
{
	TreeMap<Double,Integer> smAtt = new TreeMap<Double,Integer>(new Comparator<Double>() {public int compare(Double o1, Double o2) {return (o2-o1>0)? 1 :-1;}});
	TreeMap<Double,Integer> smObj = new TreeMap<Double,Integer>(new Comparator<Double>() {public int compare(Double o1, Double o2) {return (o2-o1>0)? 1 :-1;}});

	protected Matrix biAdjacency = null;
	protected Matrix biAdjacencyTranspose = null;
	protected Matrix adjacency = null;
	protected Matrix degree = null;
	protected Matrix laplacian = null;
	protected Matrix pseudoInverse = null;
	protected Matrix resistance = null;
	protected Matrix [] eigenDecomp = null;
	protected Matrix invertedDiagonal = null;

	public JamaResistanceLayout(){}

	public void buildStructuresFromContext(Context context)
	{
		biAdjacency = convertContextToMatrix(context);
	}

	public void buildStructuresFrom2DArray (double [][] array)
	{
		biAdjacency = Matrix.constructWithCopy(array);
	}

	public void buildStructuresFromFile(String file) 
	{
		ClassLoader classLoader = getClass().getClassLoader();
		try {
			System.out.println("reading file");

			InputStream in = classLoader.getResourceAsStream(file);
			BufferedReader b = new BufferedReader(new InputStreamReader(in));

			List<Double[]> m = new ArrayList<Double[]>();

			String line;
			while ((line = b.readLine()) != null) 
			{
				line = line.trim();
				if(line.length() == 0)	continue;
				String[] sa = line.split(" ");

				Double[] row = new Double [sa.length];

				for(int i=0; i<sa.length;i++)
					row[i] = Double.valueOf(sa[i]);

				m.add(row);
			}

			biAdjacency = new Matrix(m.size(),m.get(0).length);
			for(int i=0; i<m.size();i++)
			{
				int h = m.get(0).length;
				for(int j=0;j<h;j++)
				{
					biAdjacency.set(i,j, m.get(i)[j]);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected  void sort()
	{
		if(biAdjacency == null)
			return;

		int colCount = biAdjacency.getColumnDimension();
		int rowCount = biAdjacency.getRowDimension();

		//construct the adjacency matrix from the context
		biAdjacencyTranspose = biAdjacency.copy().transpose();

		int size = colCount+rowCount;

		adjacency = new Matrix(size,size);

		System.out.println(colCount+":"+rowCount);

		adjacency.setMatrix(0, rowCount-1, rowCount, rowCount+colCount-1, biAdjacency);
		adjacency.setMatrix(rowCount, rowCount+colCount-1, 0, rowCount-1, biAdjacencyTranspose);

		// build the degree matrix ( out going edges from each att/obj) : diagonal matrix
		degree = new Matrix(size,size);

		// for each attribute add the values of the column
		int index=0;

		for(int row=0; row < rowCount; row++)
		{
			int count=0;
			for(int col=0;col<colCount;col++)
				count+= biAdjacency.get(row, col); 

			degree.set(index, index, count);
			index++;
		}

		for(int col=0; col< colCount; col++)
		{
			int count=0;
			for(int row=0;row<rowCount;row++)
				count+= biAdjacency.get(row, col); 

			degree.set(index, index, count);
			index++;
		}

		laplacian = degree.minus(adjacency);

		pseudoInverse = calcPseudoInverse(laplacian);

		// create a "delta" matrix from the PseudoInverse diagonal (replicate diagonal value in all column entries)
		Matrix delta = new Matrix(pseudoInverse.getRowDimension(), pseudoInverse.getColumnDimension());
		for(int i=0; i<laplacian.getRowDimension();i++)
		{
			for(int j=0;j<laplacian.getColumnDimension();j++)
			{
				delta.set(i,j, pseudoInverse.get(i, i));
			}
		}

		// Construct the resistance matrix from -2*LaplacianPlus + dlata + delta.transpose 
		resistance = pseudoInverse.times(-2).plus(delta).plus(delta.transpose());

		//reduce resistance matrix to a single dimension using MDS(collapse columns)
		double [][] mds = MDSJ.classicalScaling(resistance.getArray(),1);

		//Sort the resultant vector in descending order attributes and then objects 
		index = 1; 

		for(int i = 0; i<mds[0].length;i++)
		{
			if(i < rowCount)
				smObj.put(mds[0][i], index++);
			else
				smAtt.put(mds[0][i], index++ - rowCount);
		}
	}


	protected Matrix calcPseudoInverse(Matrix matrix)
	{
		EigenvalueDecomposition ed = matrix.eig();

		invertedDiagonal = ed.getD(); // the eigenvalues

		printMatrix("--------diagonal-----------------", invertedDiagonal);

		//iterate over dm, invert values (skip 0)
		for(int i=0;i<invertedDiagonal.getColumnDimension();i++)
		{
			double v = invertedDiagonal.get(i,i);
			if(v<=0.00000001)
			{
				invertedDiagonal.set(i, i, 0);
				continue;
			}

			invertedDiagonal.set(i, i, 1/v);
		}

		printMatrix("--------inverted diagonal-----------------", invertedDiagonal);


		Matrix h = ed.getV().copy().transpose();
		return ed.getV().times(invertedDiagonal).times(h);
	}


	public Collection<Integer> getAttributeOrder() {return smAtt.values();}

	public Collection<Integer> getObjectOrder() {return smObj.values();}

	public void printAllStructures()
	{
		printMatrix("biAdjacency", biAdjacency);

		printMatrix("biAdjacencyTranspose", biAdjacencyTranspose);

		printMatrix("AdjacencyMatrix", adjacency);

		printMatrix("Degree Matrix", degree);

		printMatrix("laplacian matrix",laplacian);

		if(eigenDecomp != null)
			printMatrix("diagonal matrix: ", eigenDecomp[1]);

		printMatrix("inverted diagonal", invertedDiagonal);

		printMatrix("Pseudo Inverse", pseudoInverse);

		printMatrix("Resistance", resistance);


		System.out.println("-------Attribute Order-------");
		for(Entry<Double, Integer> e : smAtt.entrySet())
			System.out.println(e.getValue()+" :  "+e.getKey());
		

		System.out.println("-------Object Order-------");
		for(Entry<Double, Integer> e : smObj.entrySet())
			System.out.println(e.getValue()+" :  "+e.getKey());
		
	}

	protected  static DecimalFormat df = new DecimalFormat("0.00000000E0");
	protected  static DecimalFormat dfm = new DecimalFormat("#####0.#########");

	protected  void printMatrix(String label, Matrix matrix)
	{
		if( label.length() >0)
			System.out.println("---------- "+label+" --------------");

		if(matrix == null)
			return;

		//get an understanding of the precision of the matrix contents
		int precision =0;
		for(int i=0;i<matrix.getRowDimension();i++)
			for(int j=0;j<matrix.getColumnDimension();j++)
				precision = Math.max(BigDecimal.valueOf(matrix.get(i,j)).precision(), precision);


		for(int i=0;i<matrix.getRowDimension();i++)
		{
			for(int j=0;j<matrix.getColumnDimension();j++)
			{
				double d = matrix.get(i,j);

				if(d<0)
					System.out.print(" ");
				else
					System.out.print("  ");

				if(precision > 2)
				{
					System.out.print(df.format(d));

					if(Math.getExponent(d) >= 0)
						System.out.print(" ");
				}
				else
					System.out.print(dfm.format(d));
			}
			System.out.println();
		}
		System.out.println();
	}

	protected  Matrix convertContextToMatrix(Context c)
	{
		double[][] m = new double[c.getObjectCount()][c.getAttrCount()];
		int obj=0;
		for(BitSet bs: c.getObjAttrs())
		{
			for(int att=0; att<bs.length();att++)
				m[obj][att] = (bs.get(att))? 1.0 : 0.0; 
			obj++;
		}
		return Matrix.constructWithCopy(m);
	}


	public static void main(String[] args)
	{
		System.out.println("Jama Resistance: Eigen");
		JamaResistanceLayout rl = new JamaResistanceLayout();
		rl.buildStructuresFromFile("./resources/Contexts/context42-clarified.txt");

		System.out.println("sorting");
		rl.sort();
		rl.printAllStructures();

	}

}
