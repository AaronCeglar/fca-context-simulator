package comparator.algortithms;

import Jama.Matrix;

public class JamaResistanceLayout2 extends JamaResistanceLayout 
{
	// avoid the use of the eigendecomp and use the alternate method TP outlined.
	
	@Override
	protected Matrix calcPseudoInverse(Matrix matrix)
	{
		Matrix m = matrix.copy(); // don't affect the matrix passed in
		// construct matrix of same size with all values set to (1/matrix.rows)
		
		for(int i=0; i<m.getRowDimension();i++)
		{
			for(int j=0;j<m.getColumnDimension();j++)
			{
				m.set(i,j, m.get(i, j)+(1.0/matrix.getRowDimension()));
			}
		}

		//invert
		Matrix q = m.inverse();

		//add negScalar
		for(int i=0; i<q.getRowDimension();i++)
		{
			for(int j=0;j<q.getColumnDimension();j++)
			{
				q.set(i,j, q.get(i, j)-(1.0/matrix.getRowDimension()));
			}
		}
		return q;
	}

	public static void main(String[] args)
	{
		System.out.println("Jama Resistance: Inverse");
		JamaResistanceLayout2 rl  = new JamaResistanceLayout2();
		rl.buildStructuresFromFile("./resources/Contexts/context42-clarified.txt");
		System.out.println("sorting");
		rl.sort();
		rl.printAllStructures();
	}

}
