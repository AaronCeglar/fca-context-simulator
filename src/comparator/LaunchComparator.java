package comparator;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import Utilities.Config;
import Utilities.Constants;
import Utilities.Context;
import Utilities.utilities;
import comparator.algortithms.Barycenter;

public class LaunchComparator 
{

	public LaunchComparator()
	{
		String persistPath = Config.get(Constants.PERSISTPATH,Constants.DEFAULTPERSISTPATH);
		Path base = Paths.get("").resolve("output").resolve(persistPath);

		try {
			Files.walkFileTree(base, new SimpleFileVisitor<Path>() 
			{
				public FileVisitResult visitFile(Path path, BasicFileAttributes arg1) throws IOException 
				{
					if(path.endsWith("context.txt"))
						compare(path);
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void compare(Path path)
	{
		Context cont = utilities.buildFormalContext(path);
		
		// run through each algorithm
		Barycenter bc = new Barycenter();
	//	bc.sort(cont);
		
		
		
		// somehow come up with a comparison heuristic
		
		
		
		
		cont.printContext();

	}


	public static void main(String[] args)
	{
		new LaunchComparator();
	}

}
