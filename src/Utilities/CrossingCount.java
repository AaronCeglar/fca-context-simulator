package Utilities;

import java.util.ArrayList;
import java.util.List;

public class CrossingCount 
{
	
	public static int calc(boolean[][] context)
	{
		int crossCount = 0;

		//calc Attr sorted array for each object. 
		List<Integer> attrEdgeSequence = new ArrayList<Integer>();
		
		for(int i=0; i<context.length; i++)
		{
			for(int j=0; j<context[i].length; j++)
			{
				if(context[i][j])
					attrEdgeSequence.add(j);
			}
		}
		
		System.out.println(attrEdgeSequence);

		// create the blank tree structure
		// balanced tree of levels that can store all attributes as leaves.
		
		int reqTreeDepth = (int)(Math.sqrt(context[0].length))+1;
		BalancedRedBlackTree tree = new BalancedRedBlackTree(reqTreeDepth);
		
		// iterate over the attrEdgeSequence
		
		// for each entry
		for(int j: attrEdgeSequence)
		{
		  // j is the columnIndex
		  		  
		  // get the corresponding leaf and visit it and all ancestors (including root) : increment visits of each node.
		  // While doing this whenever visit a left node (odd Id) add the entry in its right sibling to the number of (global)crossings
		  crossCount+= tree.visit(j);
  	}
		
		return crossCount;
		
	}

}
