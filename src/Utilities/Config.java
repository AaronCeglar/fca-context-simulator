package Utilities;
import java.io.InputStream;
import java.util.Properties;

public class Config
{
	private Properties configFile;

	private static Config instance;

	private Config() {
		configFile = new java.util.Properties();
		try {
			// need to get the resource in relation to this class not where called from
			ClassLoader classLoader = getClass().getClassLoader();
			InputStream in = classLoader.getResourceAsStream("./resources/ContextGen.cfg");
			configFile.load(in);
			in.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getValue(String key)
	{
		return configFile.getProperty(key);
	}

	public static String get(String key) 
	{
		if (instance == null) instance = new Config();
		return instance.getValue(key).trim();
	}

	public static String get(String key, String def)
	{
		if (instance == null) instance = new Config();
		try {
			String value = instance.getValue(key);
			if(value == null ||value.isEmpty() )
				return def;
			return value.trim();
		} catch (NumberFormatException e) {
			return def;
		}
	}

	public static int getInt(String key) 
	{
		if (instance == null) instance = new Config();
		try {
			return Integer.parseInt(instance.getValue(key).trim());
		} catch (NumberFormatException e) {
			return -1;
		}
	}

	public static int getInt(String key, int def)
	{
		if (instance == null) instance = new Config();
		try {
			String value = instance.getValue(key);
			if(value == null || value.isEmpty() )
				return def;
			return Integer.parseInt(value.trim());
		} catch (NumberFormatException e) {
			return def;
		}
	}

	public static double getDouble(String key, double def)
	{
		if (instance == null) instance = new Config();
		try {
			String value = instance.getValue(key);
			if(value == null || value.isEmpty() )
				return def;
			return Double.parseDouble(value.trim());
		} catch (NumberFormatException e) {
			return def;
		}
	}
	
		
	public static boolean getBool(String key, boolean def) 
	{
		if (instance == null) instance = new Config();
		String value = instance.getValue(key);
		if(value == null)
			return def;

		return Boolean.parseBoolean(value.trim());
	}

}
