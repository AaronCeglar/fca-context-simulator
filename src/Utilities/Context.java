package Utilities;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;


public class Context 
{
	Path contextPath = null;
	String name = "default";

	private List<BitSet> objAttrs = new ArrayList<BitSet>(); //Attribute bitset for each object
	private List<String> objectLabels = new ArrayList<String>();  //Object labels

	private List<BitSet> attObjs = new ArrayList<BitSet>();        //The objects pertaining to each attribute.
	private List<String> attLabels = new ArrayList<String>();         //The labels pertaining to each attribute.

	public Context(){}

	public void setPath(Path p ) { contextPath = p;}

	public void setName(String n) { name = n;}
	
	public String getName() { return name;}

	public void setObjLabels(ArrayList<String> objectLabels) 
	{
		this.objectLabels = objectLabels;
	}

	public void setObjAtts(ArrayList<BitSet> objAttBitsets) 
	{
		this.objAttrs = objAttBitsets;
	}

	public boolean setAttLabel(int index, String a) 
	{
		attLabels.set(index, a);
		if(index != attLabels.size ()-1)
			return false;
		attObjs.add (new BitSet ());
		return true;
	}

	public List<BitSet> getObjAttrs(){return objAttrs;}
	public BitSet getObjAttrs(int index) {return objAttrs.get(index);}
	public void setAttObj(int index, BitSet a) {attObjs.set(index,a);}
	
	public List<BitSet> getAttObjs(){return attObjs;}

	public void addAtt(String attribute, BitSet set)
	{
		attLabels.add(attribute);
		attObjs.add(set);
	}

	public int addObject(String s)
	{
		if (objectLabels.contains(s))
			return -1;

		objectLabels.add(s);
		objAttrs.add(new BitSet());
		return objectLabels.size()-1;
	}

	public int addObject(int i)
	{
		return addObject(String.valueOf(i));
	}

	public void addObjectAttribute(int objIndex, String attr) 
	{
		BitSet objAttsBitSet = objAttrs.get(objIndex);

		int attIndex = attLabels.indexOf(attr);
		if(attIndex == -1) // new attribute
		{
			attLabels.add(attr);
			attObjs.add(new BitSet());
			attIndex = attLabels.size()-1;
		}

		BitSet attObjBitSet = attObjs.get(attIndex);

		objAttsBitSet.set(attIndex);
		attObjBitSet.set(objIndex);
	}
	
	public void addObjectAttribute(int objIndex, int attIndex, boolean set) 
	{
		BitSet objAttsBitSet = objAttrs.get(objIndex);
		String attLabel = String.valueOf(attIndex);
		
		if(attLabels.indexOf(attLabel) <0)
		{
			attLabels.add(attLabel);
			attObjs.add(new BitSet());
		}

		if(set)
		{
		 objAttsBitSet.set(attIndex);
		 attObjs.get(attIndex).set(objIndex);
		}
	}
		
	public int getObjectCount() {return objectLabels.size();}
	public int getAttrCount() {return attLabels.size();}

	public void printContext()
	{
		System.out.println("----------");
		for(int i=0; i<objectLabels.size();i++)
		{
			System.out.print(objectLabels.get(i)+": ");
			BitSet b = objAttrs.get(i);

			for (int j = b.nextSetBit(0); j >= 0; j = b.nextSetBit(j+1)) 
			{
				System.out.print(attLabels.get(j)+" ");
			}
			System.out.println();
		}
	}

	public List<String> getAttLabels() { return attLabels;}
	public List<String> getObjLabels() { return objectLabels;}

}
