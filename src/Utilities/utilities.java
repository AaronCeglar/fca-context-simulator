package Utilities;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;

public class utilities
{

	@SuppressWarnings("unchecked")
	public static int[] shuffledCopy(int[] original) {
		int originalLength = original.length; // For exception priority compatibility.
		Random random = new Random();
		int[] result = (int[]) Array.newInstance(original.getClass().getComponentType(), originalLength);

		for (int i = 0; i < originalLength; i++) {
			int j = random.nextInt(i+1);
			result[i] = result[j];
			result[j] = original[i];
		}

		return result;
	}

	public static String calcColumnLabel(int number) // column Labels
	{
		int j = number/26;
		int k = number%26;

		if (k==0)
		{
			k=26;
			j--; 
		}

		if (j>=1)
			return calcColumnLabel(j) + String.valueOf((char)(k+96));

		return String.valueOf((char)(k+96));
	}


	public static boolean[][] manualInitialiseMatrix()
	{
		boolean[][] matrix = new boolean[5][5];

		matrix[0][0] = true;
		matrix[0][1] = true;
		matrix[0][2] = true;
		matrix[1][2] = true;
		matrix[1][3] = true;
		matrix[2][3] = true;
		matrix[2][4] = true;
		matrix[2][5] = true;
		matrix[2][6] = true;
		matrix[3][6] = true;
		matrix[3][7] = true;
		matrix[4][7] = true;
		matrix[4][8] = true;
		return matrix;
	}

	public static void deleteDirectory(Path directory) throws IOException
	{
		if (Files.exists(directory))
		{
			Files.walkFileTree(directory, new SimpleFileVisitor<Path>()
			{
				@Override
				public FileVisitResult visitFile(Path path, BasicFileAttributes basicFileAttributes) throws IOException
				{
					Files.delete(path);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path directory, IOException ioException) throws IOException
				{
					Files.delete(directory);
					return FileVisitResult.CONTINUE;
				}
			});
		}
	}

	/**
	 * Construct the Formal context from the input file.
	 * @param file
	 * @calledBy ConceptExplorer
	 */
	public static Context buildFormalContext(Path path)
	{
		Context context = new Context(); 
		Hashtable<String, AttributeHolder> attributeHash = new Hashtable<String, AttributeHolder>();
		ArrayList<BitSet> objAttrs = new ArrayList<BitSet>(); //Attribute bitset for each object
		ArrayList<String> objectLabels = new ArrayList<String>();        //Object labels

		int colCount = 0;                    //Incremented: relates to attribute identifier.
		int rowCount = 0;                    //Incremented: relates to object identifier.
		AttributeHolder attribute = null;    //An attribute

		try {
			String line = null;
			String[] trans = null;
			context.setPath(path);

			BufferedReader in = Files.newBufferedReader(path);
			while ((line = in.readLine()) != null) {
				trans = line.split("\\s+");
				objectLabels.add(trans[0]);

				BitSet row = new BitSet();
				for (int i = 1; i < trans.length; i++) {
					//If a new attribute then create a new column Hash entry, else append to attribute id to row bitset.
					if ((attribute = attributeHash.get(trans[i])) == null) {
						row.set(colCount);
						attributeHash.put(trans[i], new AttributeHolder(trans[i], colCount++, rowCount));
					} else {
						row.set(attribute.colIndex);
						attribute.set.set(rowCount);
					}
				}
				objAttrs.add(row);
				rowCount++;
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("Formal Context: File Not Found");
		} catch (IOException e) {
			System.out.println("Formal Context: Parsing Error");
		} 

		//Set object storage structures
		context.setObjLabels(objectLabels);
		context.setObjAtts(objAttrs);

		//Set attribute storage structures
		TreeSet<AttributeHolder> ts = new TreeSet<AttributeHolder>(
				new Comparator<AttributeHolder>() {
					public int compare(AttributeHolder a, AttributeHolder b){
						int index = (a.colIndex - b.colIndex);
						if (index != 0) return index;
						return 1;
					}
				});
		ts.addAll(attributeHash.values());

		Iterator<AttributeHolder> ite = ts.iterator();
		while (ite.hasNext()) {
			AttributeHolder c = ite.next();
			context.addAtt(c.attribute, c.set);
		}
		return context;
	}
}


/**
 * Attribute holder is used as a temporary storage structure during creation of the formal context matrix.
 */
class AttributeHolder extends Object 
{
	public BitSet set = null;  //Attribute objects.   
	public int colIndex = 0;      //Attribute column index
	public String attribute = ""; //Attribute name
	
	public AttributeHolder(String a, int i, int r) 
	{
		set = new BitSet(i);
		colIndex = i;
		attribute = a;
		set.set(r);
	}
}