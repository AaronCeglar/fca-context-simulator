package Utilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.tree.TreeNode;

public class BalancedRedBlackTree  
{
	Node root = new Node(0,0);
	int maxDepth = -1;
	ConcurrentLinkedQueue<Node> queue = new ConcurrentLinkedQueue<Node>();
	
	List<Node> leaves = new ArrayList<Node>();
	
	public BalancedRedBlackTree(int d)
	{
		// build blank tree structure
		// each leaf (left to right) should be stored in a list for easy access
		maxDepth = d;
		
		queue.add(root);
		buildTree();
		//printBFSTree();
	}
	
	
  // breadthfirstContruction
	private void buildTree()
	{
	  int id = 1;
	  
	  while(! queue.isEmpty())
	  {
	    Node n = queue.poll();
	    if(n == null)
	      return;
	    
	    Node leftChild = new Node(id++, n.getDepth()+1);
	    Node rightChild = new Node(id++, n.getDepth()+1);
	    
	    n.add(leftChild);
	    n.add(rightChild);
	    
	    if(leftChild.getDepth() < maxDepth)
	    {
	      queue.add(leftChild);
	      queue.add(rightChild);
	    }
	    else
	    {
	      leaves.add(leftChild);
	      leaves.add(rightChild);
	    }

	  }
	}
	

  public int visit(int leafIndex)
  {
    Node n = leaves.get(leafIndex);
    int count = 0;
    
    while (n != null)
    {
      n.incVisits();
      if(n.isLeft)
        count += ((Node)n.getNextSibling()).getVisitCount();
      n = (Node)n.getParent();
    }
    return count;
  }
	
	  
  private void printDFSTree() 
  {
    Iterator<TreeNode> d = root.depthFirstEnumeration().asIterator();
    
    while(d.hasNext())
    {
      Node f = (Node)d.next();
      for(int i =0; i<f.depth;i++)
        System.out.print("  ");
      System.out.println(f.id);
    }
  }

  private void printBFSTree() 
  {
    Iterator<TreeNode> d = root.breadthFirstEnumeration().asIterator();
    while(d.hasNext())
    {
      Node f = (Node)d.next();
      for(int i =0; i<f.depth;i++)
        System.out.print("  ");
      System.out.println(f.id);
    }
  }
 		
	
	public static void main(String[] args)
  {
    new BalancedRedBlackTree(4);
  }


	
}
