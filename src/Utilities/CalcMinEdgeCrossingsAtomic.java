package Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;

public class CalcMinEdgeCrossingsAtomic
{
  int minCrossCount = Integer.MAX_VALUE;
  int maxCrossCount = Integer.MIN_VALUE;
  int count =0;
  
  int maxCount = 1000;
  
  
  public CalcMinEdgeCrossingsAtomic()
  {
    
    ClassLoader classLoader = getClass().getClassLoader();
    try {
            
      InputStream in = classLoader.getResourceAsStream("./resources/Contexts/context42-atomic.txt");
      BufferedReader b = new BufferedReader(new InputStreamReader(in));

      Context context = new Context();
      
      String line;
      int objID =0;
      while ((line = b.readLine()) != null) 
      {
        if( line.trim().length() == 0)
          continue;
        System.out.println(line);
        String[] sa = line.split(" ");

        int index = context.addObject(objID++);
        if(index >= 0)
        {
          for(int i=1; i<sa.length;i++)
          {  
             if(Math.round(Float.valueOf(sa[i]))>0)
             {
                context.addObjectAttribute(index,String.valueOf(i));
             }
          }
        }
      }

      context.printContext();
      
      
      //Context is built
      //get the sets of object and att permutations
      //for each permutation build the binaryArray 
      
      
      System.out.println(context.getObjLabels());
      System.out.println(context.getAttLabels());
      
      
      System.out.println(context.getObjectCount());
      System.out.println(context.getAttrCount());
      
      System.out.println("-----------------");
      
      int objPermTestCount =0;
      
      for(Iterator<int[]> objIt = new Permute(context.getObjectCount()); objIt.hasNext(); ) 
      {
        int[] objPerm = objIt.next();
        
        System.out.println(objPermTestCount);
        if(objPermTestCount++ > maxCount)
          break;
       
       // System.out.println(Arrays.toString(objPerm));
        int attPermTestCount=0;
        
        for(Iterator<int[]> attIt = new Permute(context.getAttrCount()); attIt.hasNext(); ) 
        {
          int[] attPerm = attIt.next();
          
          if(attPermTestCount++ > maxCount)
            break;
          
         // System.out.println("  -"+Arrays.toString(attPerm));
        
          int crossCount = 0;

          List<Integer> attrEdgeSequence = new ArrayList<Integer>();

          //iterate over the perm of objects, and add the index of each perm'd att to the array.
          for(int o = 0; o< objPerm.length;o++)
          {
            int objIndex = objPerm[o];
            BitSet objAttrs = context.getObjAttrs(objIndex);
            
            // iterate of the perm order of atts adding them to the EdgeSequence  
            for(int a = 0; a< attPerm.length;a++)
            {
              int attIndex = attPerm[a];
              if(objAttrs.get(attIndex))
                attrEdgeSequence.add(a);
            }
          }
          
 
        // System.out.println(attrEdgeSequence);

          // create the blank tree structure
          // balanced tree of levels that can store all attributes as leaves.
          
          int reqTreeDepth = (int)(Math.sqrt(attPerm.length))+1;
          BalancedRedBlackTree tree = new BalancedRedBlackTree(reqTreeDepth);
          
          // iterate over the attrEdgeSequence
          
          // for each entry
          for(int j: attrEdgeSequence)
          {
            // j is the columnIndex
                  
            // get the corresponding leaf and visit it and all ancestors (including root) : increment visits of each node.
            // While doing this whenever visit a left node (odd Id) add the entry in its right sibling to the number of (global)crossings
            crossCount+= tree.visit(j);
          }
          
         // System.out.println(crossCount);

          if(crossCount < minCrossCount) minCrossCount = crossCount;
          if(crossCount > maxCrossCount) maxCrossCount = crossCount;
          
          
          // now count crossings for the perm

        }
      }
      
      System.out.println("min edge crossing: "+ minCrossCount);
      System.out.println("max edge crossing: "+ maxCrossCount);


    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    

  }
  
  
  public static void main(String[] args) 
  {
     new CalcMinEdgeCrossingsAtomic();
  

  }

}
