package Utilities;

import javax.swing.tree.DefaultMutableTreeNode;

public class Node extends DefaultMutableTreeNode
{
  private static final long serialVersionUID = 1L;
  int id = -1;
  boolean isLeft = false;
  int depth=0;
  int visits=0; 
  
  public Node (int id, int depth)
  {
    super();
    this.id = id;
    this.depth = depth;
    isLeft = ((id%2)!=0) ? true : false;
  }

  public boolean isRoot() { return this.parent == null;}
  
  public int getDepth() {return depth;}
  
  public int getVisitCount() {return visits;}
  public void incVisits() { visits++;}
}
