package Utilities;
public class Constants
{
	final public static String CONTEXTS = "contexts";
	final public static int DEFAULTCONTEXTS = 5;

	final public static String MINCOLUMNS = "minColumns";
	final public static int DEFAULTMINCOLUMNS = 5;

	final public static String MAXCOLUMNS = "maxColumns";
	final public static int DEFAULTMAXCOLUMNS = 10;
	
	final public static String CONNECTBASE = "connectBase";
	final public static int DEFAULTCONNECTBASE = 1;
	
	final public static String CONNECTVARIANCE = "connectVariance";
	final public static double DEFAULTCONNECTVARIANCE = 1.5;
	
	//-------------------------------------
		
	final public static String MINADDEDGES = "minAddEdges";
	final public static int DEFAULTMINADDEDGES = 3;
	
	final public static String MAXADDEDGES = "maxAddEdges";
	final public static int DEFAULTMAXADDEDGES = 5;
	
	final public static String MINCROSSING = "minCrossing";
	final public static int DEFAULTMINCROSSING = 1;
	
	final public static String MAXCROSSING = "maxCrossing";
	final public static int DEFAULTMAXCROSSING = 1;
	
	//-------------------------------------
	
	final public static String MAXATTEMPTS = "maxAttempts";
	final public static int DEFAULTMAXATTEMPTS = 20;
	
	final public static String MAXCONITIGUOUSFAILS = "maxFails";
	final public static int DEFAULTMAXCONTIGUOUSFAILS = 20;
	
	final public static String DUPLICATEPARAMETERS = "duplicateContextPartameters";
	final public static boolean DEFAULTDUPLICATCONTEXTPARAMETERS = false;
		
	final public static String PERSISTPATH = "persistPath";
	final public static String DEFAULTPERSISTPATH = "run";
	
	final public static String PERSISTOVERWRITE = "persistOverwrite";
	final public static boolean DEFAULTPERSISTOVERWRITE = true;
}
