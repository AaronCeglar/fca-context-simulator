package generator;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import Utilities.utilities;

public class Builder 
{
	int[] rowLabels = null; // numbers
	int[] colLabels = null;  // letters

	boolean[][] initialMatrix = null;
	boolean[][] matrix = null;

	int[] shuffledRows = null;
	int[] shuffledColumns = null;
	boolean [][] shuffledMatrix = null;

	Set<Cell> crossings = new TreeSet<Cell>();

	public Builder() {}

	public void build(int columns, int connectBase, double connectVariance)
	{
		initialiseContext(columns, connectBase, connectVariance);
		//utilities.manualInitialiseMatrix();
		initialiseLabels();
		
		System.out.println("---IM----------");
		printMatrix(initialMatrix);
	}

	private void initialiseContext(int columns, int connectBase, double connectVariance)
	{
		Random rand = new Random();
		int currentIndex = 0; 

		List<boolean[]> Mat = new ArrayList<boolean[]>();

		while(currentIndex < columns-1)
		{
			// create a new row
			boolean[] uu = new boolean[columns];
			for(int j=0; j<columns; j++)
				uu[j] = false;

			int connections = (int)((Math.abs(rand.nextGaussian())*connectVariance)+connectBase);
			if(connections <2)
				connections =2;
			
			connections=2;

			for(int j=0;j<connections;j++)
			{
				if (currentIndex < columns)
					uu[currentIndex++]=true; 
			}
			currentIndex--;
			Mat.add(uu);
		}

		matrix = new boolean[Mat.size()][columns];
		initialMatrix =  new boolean[Mat.size()][columns];

		for(int i=0;i<Mat.size();i++)
		{
			boolean [] b = Mat.get(i);
			matrix[i] = b;

			for(int j=0;j<b.length;j++)
			{
				initialMatrix[i][j] = b[j];
			}
		}
	}


	private void initialiseLabels()
	{
		rowLabels = new int[matrix.length];
		for( int i=0;i<rowLabels.length; i++)
			rowLabels[i] = i;

		// store columnLabels as int as well for permute purposes and then reassign.
		colLabels = new int[matrix[0].length];
		for(int i=0;i<colLabels.length; i++)
			colLabels[i] = i;
	}

	public boolean addEdges(int requiredEdges, int minCrossings, int maxCrossings, int maxAttempts)
	{
		int attempt = 0;
		do { // an attempt
			System.out.println("rr");
			if (addEdgesAttempt(requiredEdges, minCrossings, maxCrossings))
			{
				return true;
			}
			// remove from matrix before clear;

			for(Cell c: crossings)
				matrix[c.row][c.column] = false;

			crossings.clear();
		} while (attempt++ < maxAttempts);
		return false;
	}

	private boolean addEdgesAttempt(int requiredEdges, int minCrossings, int maxCrossings) 
	{
		while (crossings.size() < requiredEdges) // for the number of edges we want crossings for.
		{
			System.out.println("ss");
			Cell result = generateEdge(minCrossings, maxCrossings);
			if (result.isValid())
			{
				//System.out.println("  edge added: "+result.toString()); System.out.println();
				// need to add here
				matrix[result.row][result.column] = true;
				crossings.add(result);
			}
			else
				return false;
		}
		
		System.out.println("----------------");
		printMatrix(matrix);
		

		//if no universals
		if(hasUniversals())
		{
			System.out.println("Universals found");
			return false;
		}

		// if no equivalence classes
		if(hasEquivalenceClasses())
		{
			System.out.println("equivalence class found");
			return false;
		}

		//if(hasSubsumption())
			//return false;

		return true;
	}

	private boolean hasSubsumption() 
	{
		// if a row is a subset of any other
		for(int i=0;i<matrix.length;i++) //rows
		{
			for(int j=i+1;j<matrix.length;j++)
			{
				if (isRowSubsumed(matrix[i], matrix[j]))
					return true;
			}
		}

		for(int i=0;i<matrix[0].length;i++) // columns
		{
			for(int j=i+1;j<matrix[0].length;j++)
			{
				if (isColumnSubsumed(i,j))
					return true;
			}
		}
		return false;
	}



	private boolean isRowSubsumed(boolean[] row, boolean[] row1)
	{
		// if for every row[i], row1[i] is true then row1 subsumes (or equal to) row
		// search for true to return early

		boolean subsumed = true;				
		for(int colIndex=0; colIndex<row.length; colIndex++)
		{
			if(row[colIndex] && !row1[colIndex]) 
				subsumed = false;
		}	
		if (subsumed) 
		{
		//	System.out.println(BooleanArrayToString(row1)+" r-subsumes "+ BooleanArrayToString(row));
		//	System.out.println("-----------");
			return true;
		}

		subsumed = true;				
		for(int colIndex=0; colIndex<row.length; colIndex++)
		{
			if(row1[colIndex] && !row[colIndex]) 
				subsumed = false;
		}	
		if (subsumed)
		{
		//	System.out.println(BooleanArrayToString(row)+" r-subsumes "+ BooleanArrayToString(row1));
		//	System.out.println("-----------");
			return true;
		}

		return false;
	}

	private boolean isColumnSubsumed(int col, int col1) 
	{
		// if for every col[i], col1[i] is true then col1 subsumes (or equal to) col
		// search for true to return early
		boolean subsumed = true; 
		for(int i=0; i<matrix.length;i++) // for each row
		{
			if(matrix[i][col] && !matrix[i][col1])
				subsumed = false;
		}
		if (subsumed) 
		{
			//System.out.println(BooleanArrayToString(col1)+" r-subsumes "+ BooleanArrayToString(col));
			//	System.out.println("-----------");
			return true;
		}

		subsumed = true; 
		for(int i=0; i<matrix.length;i++) // for each row
		{
			if(matrix[i][col1] && !matrix[i][col])
				subsumed = false;
		}
		if (subsumed)
		{
				//System.out.println(BooleanArrayToString(col)+" r-subsumes "+ BooleanArrayToString(col1));
				//System.out.println("-----------");
			return true;
		}

		return false;
	}



	private boolean hasEquivalenceClasses() 
	{
		//pairwise comparison of rows and columns

		for(int i=0;i<matrix.length;i++) //rows
		{
			for(int j=i+1;j<matrix.length;j++)
			{
				if (areRowsSame(matrix[i], matrix[j]))
					return true;
			}
		}

		for(int i=0;i<matrix[0].length;i++) // columns
		{
			for(int j=i+1;j<matrix[0].length;j++)
			{
				if (areColumnsSame(i,j))
					return true;
			}
		}
		return false;
	}

	private boolean areRowsSame(boolean[] row, boolean[] row2) 
	{
		for(int colIndex=0; colIndex<row.length; colIndex++)
		{
			if(row[colIndex] != row2[colIndex]) //boolean
				return false;
		}	
		return true;
	}

	private boolean areColumnsSame(int col, int col2)
	{
		for(int i=0; i<matrix.length;i++) // for each row
		{
			if(matrix[i][col] != matrix[i][col2])
				return false;
		}
		return true;
	}

	private boolean hasUniversals()
	{
		for(int i=0;i<matrix.length;i++) // for each row
		{
			if(isRowFull(matrix[i])) // is the row full
				return true;
		}

		for(int i=0;i<matrix[0].length;i++) // for each column
		{
			if (isColumnFull(i)) // is the column full
				return true;
		}
		return false;
	}

	private boolean isRowFull(boolean[] row)
	{
		for(int colIndex=0; colIndex<row.length; colIndex++) //iterate over columns
		{
			if(!row[colIndex]) //boolean
				return false;
		}	
		return true;
	}

	private boolean isColumnFull(int colIndex)
	{
		for(int rowIndex=0; rowIndex<matrix.length; rowIndex++) // iterate over rows
		{
			if(!matrix[rowIndex][colIndex]) //is the jth column of each row full?
				return false;;
		}	
		return true;
	}


	private Cell generateEdge(int minCrossings, int maxCrossings)
	{
		// initialise a row elimination structure (0 to matrix.length-1)
		ArrayList<Integer> rowsToTest = new ArrayList<Integer>();
		for(int i=0;i<matrix.length;i++)
			rowsToTest.add(i);

		while(! rowsToTest.isEmpty())
		{
			System.out.println("ww");
			int randRowIndex = new Random().nextInt(rowsToTest.size()); 
			int testRow = rowsToTest.get(randRowIndex);

			boolean [] targetRow = matrix[testRow];

	
			rowsToTest.remove(randRowIndex);
			
			ArrayList<Integer> columnsToTest = new ArrayList<Integer>();
			for(int i=0;i<matrix[0].length;i++)
				columnsToTest.add(i);

				
			while(! columnsToTest.isEmpty())	
			{
				System.out.println("vv");
			//	System.out.println(columnsToTest);
				int randColIndex = new Random().nextInt(columnsToTest.size()); 
				int testColumn = columnsToTest.get(randColIndex);
				
				if (targetRow[testColumn]) continue; // if isX then skip 

				// count all bits in the matrix columns between candidateEdge (exclusive) and firstSetBit (inclusive) but not including self
				// if equal to crossings then set and roll a new one
				if(testColumn<getFirstSetBit(targetRow))// before X sequence
				{
					if (calcCandEdgeCrossPrior(testRow, testColumn, minCrossings, maxCrossings)) // no not boolean must be exact (int)
						return new Cell(testRow, testColumn);
				}
				else // must be greater then lastSetBit 
				{
					if (calcCandEdgeCrossPost(testRow, testColumn, minCrossings, maxCrossings))
						return new Cell(testRow, testColumn);
				}
				
				columnsToTest.remove(randColIndex);
			}
		}
		// return the valid crossing cell.
		return new Cell();
	}


	public void randomise()
	{
		shuffledRows = utilities.shuffledCopy(rowLabels);
		shuffledColumns = utilities.shuffledCopy(colLabels);

		boolean [][] shuffledRowMatrix = new boolean[rowLabels.length][colLabels.length];
		for(int i=0; i<shuffledRows.length;i++)
		{
			boolean[] nextRow = matrix[shuffledRows[i]].clone(); // get the row to add to the shuffled deck, can clone as shallow structure. 
			shuffledRowMatrix[i] = nextRow;
		}

		shuffledMatrix = new boolean[rowLabels.length][colLabels.length];
		for(int shuffledColumnIndex=0; shuffledColumnIndex<shuffledColumns.length;shuffledColumnIndex++) //iterate over the shuffled column labels
		{
			int nextColumn = shuffledColumns[shuffledColumnIndex]; // get the next shuffled column labels 

			for(int rowIndex=0; rowIndex<rowLabels.length; rowIndex++) // for each row
				shuffledMatrix[rowIndex][shuffledColumnIndex] = shuffledRowMatrix[rowIndex][nextColumn]; 
		}
	}


	public int getColumnCount()	{ return matrix[0].length;}
	public int getRowCount() { return matrix.length;}
	public int getCrossingsCount() {return crossings.size();}
	public Set<Cell> getCrossings() {return crossings;}


	private boolean calcCandEdgeCrossPrior(int row, int testColumn, int minCross, int maxCross)
	{
		int crossCount=0;
		for(int compRow = 0; compRow<row; compRow++) // iterate over rows
		{
			for(int col=testColumn+1;col<matrix.length-1;col++) // for the valid columns increment crossCount, drop when the new edge would exceed the maxCross. 
			{
				if (matrix[compRow][col]) crossCount++;  
				if (crossCount > maxCross) 
					return false;
			}
		}

		if (crossCount >= minCross)
			return true;
		return false;
	}


	private boolean calcCandEdgeCrossPost(int row, int testColumn, int minCross, int maxCross)
	{
		int crossCount=0;
		for(int compRow = matrix.length-1; compRow>row; compRow--) // iterate over rows
		{
			for(int col=testColumn-1;col>=0;col--) // for the valid columns increment crossCount, drop when the new edge would exceed the maxCross. 
			{
				if (matrix[compRow][col]) crossCount++;  
				if (crossCount > maxCross) 
					return false;
			}
		}
		return true; // crossCount <= maxCross
	}


	private int getLastSetBit(boolean[] rw) 
	{
		for(int h=rw.length-1;h>0;h--)
		{
			if (rw[h])
				return h;
		}
		return 0;
	}


	private int getFirstSetBit(boolean[] r)
	{
		for(int h=0;h<r.length;h++)
		{
			if (r[h])
				return h;
		}
		return r.length-1;
	}



	private void printMatrix(boolean[][] matrix)
	{
		for(int i=0; i<matrix.length; i++)
		{
			System.out.print("  ");
			for(int j=0; j<matrix[i].length; j++)
				System.out.print((matrix[i][j])?"1":"0");
			System.out.println();
		}
	}

	private String BooleanArrayToString(boolean[] array)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("[");

		for(int j=0; j<array.length; j++)
			sb.append((array[j])?"1":"0");

		sb.append("]");
		return sb.toString();
	}
	
	private String BooleanArrayToString(int col)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("[");

		for(int j=0; j<matrix.length; j++)
			sb.append((matrix[j][col])?"1":"0");

		sb.append("]");
		return sb.toString();
	}
	


	private void printMatrixWithLabels(boolean[][] matrix, int[] rLabels, int[] cLabels)
	{
		int rLabelSpace = String.valueOf(rLabels.length).length();
		for(int i=0; i<rLabelSpace+3; i++)
			System.out.print(" ");
		for(int i=0;i<cLabels.length;i++)
			System.out.print(utilities.calcColumnLabel(cLabels[i]+1));
		System.out.println();
		for(int i=0; i<matrix.length; i++)
		{
			System.out.print("  "+rLabels[i]);
			for(int j=0; j<rLabelSpace- String.valueOf(rLabels[i]).length()+1; j++)
				System.out.print(" ");
			for(int j=0; j<matrix[i].length; j++)
				System.out.print((matrix[i][j])?"1":"0");
			System.out.println();
		}
	}


	private void printMatrixWithLabelstoWriter(PrintWriter meta, boolean[][] matrix, int[] rLabels, int[] cLabels)
	{
		int rLabelSpace = String.valueOf(rLabels.length).length();
		for(int i=0; i<rLabelSpace+3; i++)
			meta.print(" ");
		for(int i=0;i<cLabels.length;i++)
			meta.print(utilities.calcColumnLabel(cLabels[i]+1));
		meta.println();
		for(int i=0; i<matrix.length; i++)
		{
			meta.print("  "+rLabels[i]);
			for(int j=0; j<rLabelSpace- String.valueOf(rLabels[i]).length()+1; j++)
				meta.print(" ");
			for(int j=0; j<matrix[i].length; j++)
				meta.print((matrix[i][j])?"1":"0");
			meta.println();
		}
	}


	public void printContext(boolean b) 
	{
		if(b)
			printMatrixWithLabels(matrix,rowLabels,colLabels);
		else
			printMatrix(matrix);
	}


	public void printShuffledContext(boolean b) 
	{
		if(b)
			printMatrixWithLabels(shuffledMatrix,shuffledRows,shuffledColumns);
		else
			printMatrix(matrix);
	}


	public void printContextWithLabelstoWriter(PrintWriter meta)
	{
		printMatrixWithLabelstoWriter(meta,matrix,rowLabels,colLabels);
	}

	public void printShuffledMatrixtWithLabelstoWriter(PrintWriter meta)
	{
		printMatrixWithLabelstoWriter(meta,shuffledMatrix,shuffledRows,shuffledColumns);
	}

	public void printInitialContextWithLabelstoWriter(PrintWriter meta)
	{
		printMatrixWithLabelstoWriter(meta,initialMatrix,rowLabels,colLabels);
	}

	public void printContext(PrintWriter cont)
	{
		for(int i=0; i<shuffledMatrix.length; i++) // iterate over matrix 
		{
			StringBuffer sb = new StringBuffer();
			sb.append(shuffledRows[i]+" ");

			for(int j=0; j<shuffledMatrix[i].length; j++) // iterate over row
			{
				if(shuffledMatrix[i][j])
					sb.append(utilities.calcColumnLabel(shuffledColumns[j]+1)+" "); // no
			}
			sb.trimToSize();
			cont.println(sb);
		}
		cont.close(); 
	}

}
