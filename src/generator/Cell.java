package generator;

import Utilities.utilities;

public class Cell implements Comparable
{
	public int row = -1;
	public int column = -1;
	private boolean valid = false;

	public Cell() {}

	public Cell(int r, int c)
	{
		row = r;
		column =  c;
		valid = true;
	}

	public boolean isValid() {return valid;}

	public String toString() { return "("+row+":"+utilities.calcColumnLabel(column+1)+")";}


	public boolean equals(Object o)
	{
		if (o == this) 
			return true;
	 
		if (!(o instanceof Cell)) 
			return false;

		Cell cc = (Cell)o;
				
		return (cc.row == row) && (cc.column == column); 
	}

	public int hashCode() {
		int result = 17;
		result = 31 * (row * column);
		return result;
	}

	@Override
	public int compareTo(Object o) 
	{
		if(this.equals(o))
			return 0;
		return -1;
		
	}


}
