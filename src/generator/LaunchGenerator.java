package generator;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import Utilities.Config;
import Utilities.Constants;
import Utilities.CrossingCount;
import Utilities.utilities;

public class LaunchGenerator 
{
	public LaunchGenerator()
	{
		int contextCount=0;
		int requiredContexts = Config.getInt(Constants.CONTEXTS,Constants.DEFAULTCONTEXTS);
		int maxFails = Config.getInt(Constants.MAXCONITIGUOUSFAILS,Constants.DEFAULTMAXCONTIGUOUSFAILS);
		int failCount = 0;

		Random rand = new Random();
		System.out.println("Build a set of "+requiredContexts+ " contexts");

		String persistPath = Config.get(Constants.PERSISTPATH,Constants.DEFAULTPERSISTPATH);
		boolean persistOverwrite = Config.getBool(Constants.PERSISTOVERWRITE,Constants.DEFAULTPERSISTOVERWRITE);
		Path base = Paths.get("").resolve("output").resolve(persistPath);

		try {
			// previous run with same name, then overwrite or create new with increment. 

			if(persistOverwrite)
			{
				utilities.deleteDirectory(base);
			}else
			{
				int increment=0;
				while (Files.exists(base))
					base = base.resolve(base+"-"+increment++); 
				Files.createDirectories(base);
			}


		} catch (IOException e) {
			e.printStackTrace();
		}


		while(contextCount < requiredContexts && failCount < maxFails)
		{
			System.out.println("dd");
			int minColumns = Config.getInt(Constants.MINCOLUMNS,Constants.DEFAULTMINCOLUMNS);
			int maxColumns = Config.getInt(Constants.MAXCOLUMNS,Constants.DEFAULTMAXCOLUMNS);
			int columns = rand.nextInt(maxColumns-minColumns+1)+minColumns;

			int connectBase = Config.getInt(Constants.CONNECTBASE,Constants.DEFAULTCONNECTBASE);
			double connectVariance = Config.getDouble(Constants.CONNECTVARIANCE,Constants.DEFAULTCONNECTVARIANCE);

			Builder context = new Builder();
			context.build(columns, connectBase, connectVariance);

			int s = CrossingCount.calc(context.initialMatrix);
			System.out.println("initial Context Crossing count : "+s);

			// print original matrix here
			//	context.printContext(true);

			int minEdges = Config.getInt(Constants.MINADDEDGES,Constants.DEFAULTMINADDEDGES);
			int maxEdges = Config.getInt(Constants.MAXADDEDGES,Constants.DEFAULTMAXADDEDGES);
			int edges = rand.nextInt(maxEdges-minEdges+1)+minEdges;

			int minCrossings = Config.getInt(Constants.MINCROSSING,Constants.DEFAULTMINCROSSING);
			int maxCrossings = Config.getInt(Constants.MAXCROSSING,Constants.DEFAULTMAXCROSSING);

			int maxAttempts = Config.getInt(Constants.MAXATTEMPTS,Constants.DEFAULTMAXATTEMPTS);

			if (context.addEdges(edges, minCrossings, maxCrossings, maxAttempts))
			{
				System.out.println("ff");
				// randomise and persist the context

				s = CrossingCount.calc(context.matrix);
				System.out.println("Amended Context Crossing count : "+s);

				context.randomise();
				persistContext(base, context, minCrossings, maxCrossings);
				contextCount++;
				System.out.println("context "+ contextCount);

				s = CrossingCount.calc(context.shuffledMatrix);
				System.out.println("Shuffled Context Crossing count : "+s);

			} else
			{
				failCount++;
			}
			//System.out.println("next context");
		}

		System.out.println("- run complete: "+contextCount+" contexts generated");
	}


	private void persistContext(Path base, Builder context, int minCrossings, int maxCrossings)
	{
		try {
			String contextDir = "context-"+context.getColumnCount()+"-"+context.getCrossingsCount()+"-"+minCrossings+"-"+maxCrossings;
			Path directory = base.resolve(contextDir);

			try {
				// previous run with same name, then overwrite or create new with increment. 

				int increment=1;
				while (Files.exists(directory))
					directory = base.resolve(contextDir+"-"+utilities.calcColumnLabel(increment++)); 
				Files.createDirectories(directory);
			} catch (IOException e) {
				e.printStackTrace();
			}

			PrintWriter meta = new PrintWriter(Files.newBufferedWriter(directory.resolve("data.txt")));

			meta.println("columns = "+ context.getColumnCount());
			meta.println("rows = "+ context.getRowCount()); 
			meta.println("introduced edges = "+ context.getCrossingsCount()); 
			meta.println("min crossings per new edge = "+ minCrossings); 
			meta.println("max crossings per new edge = "+ maxCrossings); 
			meta.println("added edges = "+ context.getCrossings()); 

			meta.println();
			meta.println("initial context");
			meta.println("----------------");

			context.printInitialContextWithLabelstoWriter(meta);

			int s = CrossingCount.calc(context.initialMatrix);
			meta.println("initial Context Crossing count : "+s);


			meta.println();
			meta.println("optimal context");
			meta.println("----------------");

			context.printContextWithLabelstoWriter(meta);

			s = CrossingCount.calc(context.matrix);
			meta.println("Amended Context Crossing count : "+s);

			meta.println();
			meta.println("randomised context");
			meta.println("----------------");

			context.printShuffledMatrixtWithLabelstoWriter(meta);
			meta.println();

			s = CrossingCount.calc(context.shuffledMatrix);
			meta.println("Shuffled Context Crossing count : "+s);

			meta.println("nb: see sibling \"context \" file for randomised context in required format.");

			meta.close();

			PrintWriter cont = new PrintWriter(Files.newBufferedWriter(directory.resolve("context.txt")));
			context.printContext(cont);
			cont.close(); 

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void main(String[] args)
	{
		new LaunchGenerator();
	}
}
